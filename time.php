<!DOCTYPE HTML>
<head>
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
<h1 id='title'>Time</h1>

<table>
  <tr>
<?php
for ($i = 0; $i <= 11; $i++) {
    echo "<tr>\r\n";
    echo "\t<th>Timezone </th> \r\n";
    echo "\t<th>Time</th> \r\n";
    echo "\t<th>Offset</th> \r\n";
    echo "</tr> \r\n";

    echo "<tr> \r\n";
    echo "\t<td id=timezone_$i>timezone</td> \r\n";
    echo "\t<td id=time_$i>time</td> \r\n"; 
    echo "\t<td id=offset_$i>offset</td> \r\n";
    echo "</tr> \r\n";
}
?>
</table>

<script src="https://momentjs.com/downloads/moment.min.js"> </script>
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"> </script>
<script src="http://momentjs.com/downloads/moment-timezone.min.js"> </script>
<script src="https://momentjs.com/downloads/moment-timezone-with-data-2012-2022.min.js"></script>
<script src="https://momentjs.com/downloads/moment-timezone-with-data.min.js"></script>


<script>
//-- defining unix timestamp as var and selecting timezones from global array to a 2d array --//
var timestamp = Date.now();

var times_array =
[
  moment.tz.names()[146], //los angeles
  moment.tz.names()[104], //denver
  moment.tz.names()[168], //new york
  moment.tz.names()[448], //lisbon
  moment.tz.names()[452], //madrid
  moment.tz.names()[486], //zurich
  moment.tz.names()[457], //moscow
  moment.tz.names()[245], //bangkok
  moment.tz.names()[308], //shanghai
  moment.tz.names()[318], //tokyo
  moment.tz.names()[358], //perth
  moment.tz.names()[361], //sydney
]; 
//-------------------------------------------------------------------------------------------//


//-- Giving array length a var --//
var times_array_length = times_array.length;
//------------------------------//


//-- Getting documents by Id with php and output them --//

<?php
  
  echo "var cell_timezone, cell_time, cell_offset;\r\n";

  for ($i = 0; $i <= 11; $i++){
    echo "//--------------------------------------------------------------------------//\r\n";
    echo "cell_timezone = document.getElementById('timezone_${i}');\r\n";
    echo "cell_timezone.textContent = times_array[${i}];\r\n \r\n";
    echo "cell_time = document.getElementById('time_${i}');\r\n";
    echo "cell_time.textContent = moment(timestamp).tz(times_array[${i}]).format();\r\n \r\n";
    echo "cell_offset = document.getElementById('offset_${i}');\r\n";
    echo "cell_offset.textContent = moment(timestamp).tz(times_array[${i}]).format('Z');\r\n";
    echo "//--------------------------------------------------------------------------//\r\n \r\n";
  }

?>

</script>

</body>