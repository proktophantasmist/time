<!DOCTYPE HTML>
<head>
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
<h1 id='title'>Time</h1>

<table>
  <tr>
<?php
for ($i = 0; $i <= 11; $i++) {
    echo "<tr>\r\n";
    echo "\t<th>Timezone </th> \r\n";
    echo "\t<th>Time</th> \r\n";
    echo "\t<th>Offset</th> \r\n";
    echo "</tr> \r\n";

    echo "<tr> \r\n";
    echo "\t<td id=timezone_$i>timezone</td> \r\n";
    echo "\t<td id=time_$i>time</td> \r\n"; 
    echo "\t<td id=offset_$i>offset</td> \r\n";
    echo "</tr> \r\n";
}
?>
</table>

<script src="https://momentjs.com/downloads/moment.min.js"> </script>
<script src="https://momentjs.com/downloads/moment-with-locales.min.js"> </script>
<script src="http://momentjs.com/downloads/moment-timezone.min.js"> </script>
<script src="https://momentjs.com/downloads/moment-timezone-with-data-2012-2022.min.js"></script>
<script src="https://momentjs.com/downloads/moment-timezone-with-data.min.js"></script>


<script>
//-- defining unix timestamp as var and selecting timezones from global array to a 2d array --//
var timestamp = Date.now();

var times_array =
[
  moment.tz.names()[146], //los angeles
  moment.tz.names()[104], //denver
  moment.tz.names()[168], //new york
  moment.tz.names()[448], //lisbon
  moment.tz.names()[452], //madrid
  moment.tz.names()[486], //zurich
  moment.tz.names()[457], //moscow
  moment.tz.names()[245], //bangkok
  moment.tz.names()[308], //shanghai
  moment.tz.names()[318], //tokyo
  moment.tz.names()[358], //perth
  moment.tz.names()[361], //sydney
]; 
//-------------------------------------------------------------------------------------------//


//-- Giving array length a var --//
var times_array_length = times_array.length;
//------------------------------//


//-- Getting documents by Id with php and output them --//


<?php
  //-- Creating vars for each table cell for timezone --//
  for ($i = 0; $i <= 11; $i++){
    echo "var cell_timezone_${i} = document.getElementById('timezone_${i}');\r\n";
  }
  //--------------------------------------------------//
  //-- Assigning values to vars --//
  for ($i = 0; $i <= 11; $i++){
    echo "cell_timezone_${i}.textContent = times_array[${i}];\r\n";
  }
  //-----------------------------//


  //-- Creating vars for each table cell for time --//
  for ($i = 0; $i <= 11; $i++){
    echo "var cell_time_${i} = document.getElementById('time_${i}');\r\n";
  }
  //-----------------------------------------------//
  //-- Assigning values to vars --//
  for ($i = 0; $i <= 11; $i++){
    echo "cell_time_${i}.textContent = moment(timestamp).tz(times_array[${i}]).format();\r\n";
  }
  //-----------------------------//


  //-- Creating vars for each table cell for offset --//
  for ($i = 0; $i <= 11; $i++){
    echo "var cell_offset_${i} = document.getElementById('offset_${i}');\r\n";
  }
  //---------------------------------------------------//


  //-- Assigning values to vars --//
  for ($i = 0; $i <= 11; $i++){
    echo "cell_offset_${i}.textContent = moment(timestamp).tz(times_array[${i}]).format('Z');\r\n";
  }
  //-----------------------------//
?>

</script>

</body>